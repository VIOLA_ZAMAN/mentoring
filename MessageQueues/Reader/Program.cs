﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Mime;
using System.Text;
using System.Threading.Tasks;
using FileProcessingService;
using Microsoft.ServiceBus.Messaging;
using MigraDoc.DocumentObjectModel;
using MigraDoc.Rendering;
using Syncfusion.Pdf.Parsing;

namespace Reader
{
    class Program
    {
        static string curDir = Path.GetDirectoryName(Process.GetCurrentProcess().MainModule.FileName);
        static string outDir = Path.Combine(curDir, "out");
        private static int _docCounter;
        static Dictionary<Guid, List<FileCopyService.Message>> _sequenceMessages = new Dictionary<Guid, List<FileCopyService.Message>>();
        static void Main(string[] args)
        {

            var client = QueueClient.Create("queue");

            client.OnMessage(GetClientBrokerMessage);
            Console.ReadKey();

        }

        private static void GetClientBrokerMessage(BrokeredMessage brokeredMessage)
        {
            try
            {
                var message = brokeredMessage.GetBody<FileCopyService.Message>();
                List<FileCopyService.Message> previousMessages;
                if (!_sequenceMessages.TryGetValue(message.Sequence, out previousMessages))
                {
                    previousMessages = new List<FileCopyService.Message>();
                }

                previousMessages.Add(message);

                if (previousMessages.Count < message.Size)
                {
                    _sequenceMessages[message.Sequence] = previousMessages;
                    return;
                }

                var result = new List<byte>();

                for (int i = 0; i < message.Size; i++)
                {
                    var orderedMessage = previousMessages.FirstOrDefault(m => m.Position.Equals(i));
                    if (orderedMessage != null)
                        result.AddRange(orderedMessage.Body);
                }
                var docBytes = result.ToArray();
                
                PdfLoadedDocument ldoc = new PdfLoadedDocument(docBytes);
                ldoc.Save(Path.Combine(outDir, string.Format("result{0}.pdf", _docCounter)));
                _docCounter++;
                ldoc.Close();

            }
            catch (Exception ex)
            {

            }
        }
    }
}
