﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Topshelf;
using NLog;
using NLog.Config;
using NLog.Targets;

namespace FileProcessingService
{
	class Program
	{		
		static void Main(string[] args)
		{
			var curDir = Path.GetDirectoryName(Process.GetCurrentProcess().MainModule.FileName);

			var inDir = Path.Combine(curDir, "in");
			var outDir = Path.Combine(curDir, "out");
            var tempDir = Path.Combine(curDir, "tempDir");
            


            var logConfig = new LoggingConfiguration();
			var target = new FileTarget()
			{
				Name = "Def",
				FileName = Path.Combine(curDir, "log.txt"),
				Layout = "${date} ${message} ${onexception:inner=${exception:format=toString}}"
			};

			logConfig.AddTarget(target);
			logConfig.AddRuleForAllLevels(target);

			var logFactory = new LogFactory(logConfig);


			HostFactory.Run(
				conf => conf.Service<FileCopyService>(
					serv => 
					{
						serv.ConstructUsing(() => new FileCopyService(inDir, outDir, tempDir));
						serv.WhenStarted(s => s.Start());
						serv.WhenStopped(s => s.Stop());
					}
					).UseNLog(logFactory)
				);
		}
    }
}
