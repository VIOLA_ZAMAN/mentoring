﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.ServiceBus.Messaging;
using MigraDoc.DocumentObjectModel;
using MigraDoc.DocumentObjectModel.Shapes;
using MigraDoc.Rendering;
using NLog;
using Topshelf.Logging;

namespace FileProcessingService
{
    public class FileCopyService
    {
        FileSystemWatcher watcher;
        private int _messageLimit = 250000;
        string inDir;
        string outDir;
        string tempDir;
        private List<String> _filesInCurrentDoc = new List<string>();
        Thread workingThread;
        ManualResetEvent workStop;
        AutoResetEvent newFile;
        private int _previousDocId = -1;
        private DateTime _lastImageAddTime = DateTime.MinValue;
        private QueueClient _send;
        private QueueClient _receive;
        static readonly LogWriter _log = HostLogger.Get<FileCopyService>();

        public FileCopyService(string inDir, string outDir, string tempDir)
        {
            _send = QueueClient.Create("queue");
            
            this.inDir = inDir;
            this.outDir = outDir;
            this.tempDir = tempDir;

            if (!Directory.Exists(inDir))
                Directory.CreateDirectory(inDir);

            if (!Directory.Exists(outDir))
                Directory.CreateDirectory(outDir);

            if (!Directory.Exists(tempDir))
                Directory.CreateDirectory(tempDir);

            workingThread = new Thread(WorkProc);
            workStop = new ManualResetEvent(false);
            newFile = new AutoResetEvent(false);

            watcher = new FileSystemWatcher(inDir);
            watcher.Created += FileCreated;
        }

        private void WorkProc()
        {
            try
            {
                do
                {
                    foreach (var file in Directory.EnumerateFiles(inDir).OrderBy(o => o))
                    {
                        if (workStop.WaitOne(TimeSpan.Zero))
                            return;

                        if (_filesInCurrentDoc.Count > 0 && (DateTime.Now - _lastImageAddTime).TotalSeconds > 10)
                        {
                            RenderDocument();
                        }

                        if (!Regex.IsMatch(file, @".jpg|.png|.jpeg$") ||
                            _filesInCurrentDoc.Any(f => Path.GetFileName(f).Equals(Path.GetFileName(file))))
                            continue;

                        int docId; //number of current document;
                        var parts = Path.GetFileNameWithoutExtension(file).Split('_');
                        if (!int.TryParse(parts.Last(), out docId) || docId <= _previousDocId)
                            continue;

                        if (_previousDocId == -1)
                            _previousDocId = docId;

                        if (_filesInCurrentDoc.Count > 0 &&
                            (docId - _previousDocId > 1 || docId - _previousDocId < 0))
                        {
                            RenderDocument();
                        }

                        var fileInTempDirectory = Path.Combine(tempDir, Path.GetFileName(file));
                        File.Copy(file, fileInTempDirectory, true);

                        _previousDocId = docId;
                        _filesInCurrentDoc.Add(fileInTempDirectory);
                        _lastImageAddTime = DateTime.Now;
                    }
                } while (WaitHandle.WaitAny(new WaitHandle[] { workStop, newFile }, 3000) != 0);
            }
            catch (Exception exception)
            {
                _send.Send(new BrokeredMessage(exception.Message));
                _log.Info(exception.Message);
            }

        }

        private void RenderDocument()
        {
            try
            {
                var render = new PdfDocumentRenderer();

                var document = new Document();
                document.DefaultPageSetup.TopMargin = 0;
                document.DefaultPageSetup.LeftMargin = 0;
                var section = document.AddSection();
                render.Document = document;

                foreach (var file in _filesInCurrentDoc)
                {
                    var img = section.AddImage(file);
                    img.Height = document.DefaultPageSetup.PageHeight;
                    img.Width = document.DefaultPageSetup.PageWidth;
                }

                render.RenderDocument();
                
                var doc = render.PdfDocument;
                MemoryStream stream = new MemoryStream();
                doc.Save(stream);
                byte[] docBytes = stream.ToArray();

                SendByteArray(docBytes);

                foreach (var file in _filesInCurrentDoc)
                {
                    File.Delete(file);
                }

                _filesInCurrentDoc.Clear();
            }
            catch (Exception exception)
            {
                _send.Send(new BrokeredMessage(exception.Message));
                _log.Info(exception.Message);
            }
        }

        private void FileCreated(object sender, FileSystemEventArgs e)
        {
            newFile.Set();
        }

        public void Start()
        {
            _send.Send(new BrokeredMessage("Service start"));
            workingThread.Start();
            watcher.EnableRaisingEvents = true;
        }

        public void Stop()
        {
            _send.Send(new BrokeredMessage("Service stop"));
            watcher.EnableRaisingEvents = false;
            workStop.Set();
            workingThread.Join();
        }


        public void SendByteArray(byte[] docBytes)
        {
            var messages = new List<byte[]>();
            for (int i = 0; i < docBytes.Length; i++)
            {
                byte[] currentBody = docBytes.Skip(i).ToArray();
                if (currentBody.Length > _messageLimit)
                    currentBody = currentBody.Take(_messageLimit).ToArray();

                i += _messageLimit;

                messages.Add(currentBody);
            }

            var sequenceId = Guid.NewGuid();
            foreach (var body in messages)
            {
                var message = new Message
                {
                    Body = body,
                    Position = messages.IndexOf(body),
                    Sequence = sequenceId,
                    Size = messages.Count
                };
                var brokeredMessage = new BrokeredMessage(message);

                _send.Send(brokeredMessage);
            }
        }

        [Serializable]
        public class Message
        {
            public Guid Sequence { get; set; }

            public int Position { get; set; }

            public int Size { get; set; }

            public byte[] Body { get; set; }
        }
    }
}