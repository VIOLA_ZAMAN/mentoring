﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SemaphoreSlim
{
    class Program
    {
        static System.Threading.SemaphoreSlim _sem = new System.Threading.SemaphoreSlim(2);
        static void Main(string[] args)
        {
            for (int i = 1; i <= 10; i++) new Thread(Enter).Start(i);
            Console.Read();
        }
        static void Enter(object id)
        {
            Console.WriteLine(id + " wants start");
            _sem.Wait();
            Console.WriteLine(id + " is star!");           // Only three threads
            Thread.Sleep(1000 * (int)id);               // can be here at
            Console.WriteLine(id + " is stop");       // a time.
            _sem.Release();
        }
    }
}
