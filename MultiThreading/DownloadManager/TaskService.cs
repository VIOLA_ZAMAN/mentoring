﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace DownloadManager
{
    public class TaskService
    {
        public WebClient _webClient = new WebClient();
        public async Task DownloadPageAsync(string url, CancellationToken cancellationToken)
        {
            await _webClient.DownloadFileTaskAsync(new Uri(url), @"E:\123");
            cancellationToken.ThrowIfCancellationRequested();
        }
    }
}
