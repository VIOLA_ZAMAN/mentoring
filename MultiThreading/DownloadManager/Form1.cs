﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DownloadManager
{
    public partial class Form1 : Form
    {
        private TaskService _taskService;
        private string _result;
        private CancellationTokenSource _cancellationTokenSource;
        public Form1()
        {
            InitializeComponent();
            _taskService = new TaskService();

            _cancellationTokenSource = new CancellationTokenSource();            
        }

        private async void Download_Click(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(textBox1.Text))
            {
                progressBar1.Value = 0;

                _taskService._webClient.DownloadProgressChanged += new DownloadProgressChangedEventHandler(client_DownloadProgressChanged);
                _taskService._webClient.DownloadFileCompleted += new AsyncCompletedEventHandler(client_DownloadFileCompleted);
                try
                {
                    await _taskService.DownloadPageAsync(textBox1.Text, _cancellationTokenSource.Token);
                }
                catch (Exception)
                {
                }

                if (_cancellationTokenSource.IsCancellationRequested)
                {
                    MessageBox.Show("Cancel");
                    progressBar1.Value = 0;
                    _cancellationTokenSource = new CancellationTokenSource();
                }
            }
        }       

        private void Cansel_Click(object sender, EventArgs e)
        {
            _cancellationTokenSource.Cancel();
            _taskService._webClient.CancelAsync();
        }

        void client_DownloadProgressChanged(object sender, DownloadProgressChangedEventArgs e)
        {
            progressBar1.Value = e.ProgressPercentage;
        }
        void client_DownloadFileCompleted(object sender, AsyncCompletedEventArgs e)
        {
            _taskService._webClient.DownloadProgressChanged -= new DownloadProgressChangedEventHandler(client_DownloadProgressChanged);
            _taskService._webClient.DownloadFileCompleted -= new AsyncCompletedEventHandler(client_DownloadFileCompleted);

            MessageBox.Show("Completed");
        }
    }
}
