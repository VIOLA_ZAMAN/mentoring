﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GUI
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();

            getData();

            updateData();

            login();

            createDB();

            init();
        }

        public async Task getData()
        {
            await Task.Delay(1000);
            URL.Text = "Get Data";
        }

        public async Task updateData()
        {
            await Task.Delay(2000);
            URL.Text = "Update Data";
        }

        public async Task login()
        {
            await Task.Delay(3000);
            URL.Text = "Login";
        }

        public async Task createDB()
        {
            await Task.Delay(4000);
            URL.Text = "Create DB";
        }

        public async Task init()
        {
            await Task.Delay(5000);
            URL.Text = "Init";
        }

    }
}
