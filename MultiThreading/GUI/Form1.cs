﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GUI
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            Thread thread = new Thread(new ThreadStart(Splash));
            thread.Start();
            Thread.Sleep(6000);
            InitializeComponent();
            thread.Abort();
        }

        public void Splash()
        {
            Application.Run(new Form2());
        }
    }
}
