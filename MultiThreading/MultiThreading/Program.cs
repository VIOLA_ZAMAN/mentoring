﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MultiThreading
{
    class Program
    {
        static void Main(string[] args)
        {
            for (int i = 0; i < 100; i++)
            {
                ThreadPool.QueueUserWorkItem(new WaitCallback(Go), i);
            }
            Console.ReadKey();
        }

        public static void Go(object instance)
        {
            for (int i = 0; i < 1000; i++)
            {
                Console.WriteLine("Task #"+instance + " Number iteration " + i);
            }
        }
    }
}
