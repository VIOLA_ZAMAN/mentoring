﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ContinuationTask
{
    class Program
    {
        static void Main()
        {
            Task task1 = new Task(() => { Console.WriteLine("Id задачи: {0}", Task.CurrentId); });

            // задача продолжения
            Task task2 = task1.ContinueWith(Display, TaskContinuationOptions.None);

            task1.Start();

            // ждем окончания второй задачи
            task2.Wait();
            Console.WriteLine("Выполняется работа метода Main");
            Console.WriteLine();
            Task originalTask2 = Task.Run(() => { throw new Exception(); });
            Task continuationTask2 = originalTask2.ContinueWith(t =>
                Console.WriteLine("Catched exception from the task:" + t.Exception.Message),
                TaskContinuationOptions.OnlyOnFaulted);
            continuationTask2.Wait();

            Console.WriteLine();
            Task originalTask = Task.Run(() => { throw new Exception(); });
            Task continuationTask = originalTask.ContinueWith(t =>
                Console.WriteLine("Catched exception from the task:" + t.Exception.Message),
                TaskContinuationOptions.OnlyOnFaulted | TaskContinuationOptions.AttachedToParent);
            continuationTask.Wait();

            Console.WriteLine();

            var cts = new CancellationTokenSource();
            cts.Cancel();

            Task originalTask1 = Task.Run(() => { }, cts.Token);
            Task continuationTask1 = originalTask1.ContinueWith(t =>
                Console.WriteLine("The continuation is running."), TaskContinuationOptions.OnlyOnCanceled);


            try
            {
                continuationTask1.Wait();
            }
            catch (AggregateException ae)
            {
                foreach (var ie in ae.InnerExceptions)
                    Console.WriteLine("{0}: {1}", ie.GetType().Name, ie.Message);

                Console.WriteLine();
            }
            finally
            {
                cts.Dispose();
            }

            Console.WriteLine("Task {0}: {1:G}", originalTask1.Id, originalTask1.Status);
            Console.WriteLine("Task {0}: {1:G}", continuationTask1.Id,
                continuationTask1.Status);

            Console.ReadLine();
        }

        static void Display(Task t)
        {
            Console.WriteLine("Id задачи: {0}", Task.CurrentId);
            Console.WriteLine("Id предыдущей задачи: {0}", t.Id);
            Thread.Sleep(3000);
        }
    }
}