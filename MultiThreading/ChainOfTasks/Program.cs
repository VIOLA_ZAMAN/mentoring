﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChainOfTasks
{
    class Program
    {
        static void Main(string[] args)
        {
            var getData = Task.Factory.StartNew(() => {
                Random rnd = new Random();
                int[] values = new int[10];
                for (int i = 0; i <= values.Length-1; i++)
                    values[i] = rnd.Next(-100, 100);

                return values;
            });

            for (int i = 0; i < getData.Result.Length; i++)
            {
                Console.Write(getData.Result[i] + " ");
            }
            Console.WriteLine();

            var processData = getData.ContinueWith((x) => {

                Random rnd = new Random();
                int number = rnd.Next(-100, 100);
                int[] mass = x.Result;
                for (int i = 0; i <= mass.Length-1; i++)
                    mass[i] = mass[i]*number;

                return mass;
            });

            for (int i = 0; i < processData.Result.Length; i++)
            {
                Console.Write(processData.Result[i] + " ");
            }
            Console.WriteLine();

            var sortData = getData.ContinueWith((x) => {
                
                int[] mass = x.Result;
                Array.Sort(mass);
                Array.Reverse(mass);

                return mass;
            });

            for (int i = 0; i < sortData.Result.Length; i++)
            {
                Console.Write(sortData.Result[i] + " ");
            }
            Console.WriteLine();

            var avgData = processData.ContinueWith((x) => {
                int[] mass = x.Result;
                int length = x.Result.Length;
                int summ = 0;
                foreach (var i in mass)
                {
                    summ += i;
                }
                return summ/length;
            });

            Console.WriteLine(avgData.Result);
            Console.Read();
        }
    }
}
