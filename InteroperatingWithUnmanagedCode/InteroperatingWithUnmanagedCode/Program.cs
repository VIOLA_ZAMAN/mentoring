﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace InteroperatingWithUnmanagedCode
{
    class Program
    {
        const int SystemPowerInformation = 12;
        const uint STATUS_SUCCESS = 0;

        struct SYSTEM_POWER_INFORMATION
        {
            public uint MaxIdlenessAllowed;
            public uint Idleness;
            public uint TimeRemaining;
            public byte CoolingMode;
        }

        [DllImport("powrprof.dll")]
        static extern uint CallNtPowerInformation(
            int InformationLevel,
            IntPtr lpInputBuffer,
            int nInputBufferSize,
            [Out] out SYSTEM_POWER_INFORMATION spi,
            int nOutputBufferSize
            );

        [DllImport("powrprof.dll", SetLastError = true)]
        private static extern UInt32 CallNtPowerInformation(
            InformationLevel InformationLevel,
            IntPtr lpInputBuffer,
            UInt32 nInputBufferSize,
            [Out] IntPtr lpOutputBuffer,
            UInt32 nOutputBufferSize
            );

        [DllImport("powrprof.dll", SetLastError = true)]
        private static extern UInt32 CallNtPowerInformation(
            int InformationLevel,
            IntPtr lpInputBuffer,
            UInt32 nInputBufferSize,
            [Out] out ulong lpOutputBuffer,
            UInt32 nOutputBufferSize
            );


        [DllImport("Powrprof.dll", CharSet = CharSet.Auto, ExactSpelling = true)]
        public static extern bool SetSuspendState(bool hiberate, bool forceCritical, bool disableWakeEvent);


        static void Main(string[] args)
        {
            IntPtr state = Marshal.AllocCoTaskMem(Marshal.SizeOf(typeof(SYSTEM_BATTERY_STATE)));
            uint retval1 = CallNtPowerInformation(InformationLevel.SystemBatteryState,
                IntPtr.Zero, 0, state, (UInt32) Marshal.SizeOf(typeof(SYSTEM_BATTERY_STATE)));
            SYSTEM_BATTERY_STATE btState = (SYSTEM_BATTERY_STATE) Marshal.PtrToStructure(state,
                typeof(SYSTEM_BATTERY_STATE));

            SYSTEM_POWER_INFORMATION spi;
            uint retval2 = CallNtPowerInformation(
                SystemPowerInformation,
                IntPtr.Zero,
                0,
                out spi,
                Marshal.SizeOf(typeof(SYSTEM_POWER_INFORMATION))
                );
            if (retval2 == STATUS_SUCCESS)
                Console.WriteLine(spi.TimeRemaining);

            var size = Marshal.SizeOf(typeof(ulong));
            ulong state2;
            uint retval3 = CallNtPowerInformation((int) InformationLevel.LastSleepTime, IntPtr.Zero, 0, out state2,
                (UInt32) size);

            if (retval3 == STATUS_SUCCESS)
            {
                Console.WriteLine(state2);
            }

            var size2 = Marshal.SizeOf(typeof(ulong));
            ulong state3;
            uint retval4 = CallNtPowerInformation((int) InformationLevel.LastWakeTime, IntPtr.Zero, 0, out state3,
                (UInt32) size2);

            if (retval4 == STATUS_SUCCESS)
            {
                Console.WriteLine(state3);
            }

            Console.ReadLine();

            SetSuspendState(false, true, false);
        }
    }

    struct SYSTEM_BATTERY_STATE
    {
        [MarshalAs(UnmanagedType.I1)] public bool AcOnLine;
        [MarshalAs(UnmanagedType.I1)] public bool BatteryPresent;
        [MarshalAs(UnmanagedType.I1)] public bool Charging;
        [MarshalAs(UnmanagedType.I1)] public bool Discharging;
        public byte Spare0;
        public byte Spare1;
        public byte Spare2;
        public byte Spare3;
        public uint MaxCapacity;
        public uint RemainingCapacity;
        public int Rate;
        public uint EstimatedTime;
        public uint DefaultAlert1;
        public uint DefaultAlert2;
    }

    enum InformationLevel
    {
        AdministratorPowerPolicy = 9,
        LastSleepTime = 15,
        LastWakeTime = 14,
        ProcessorInformation = 11,
        ProcessorPowerPolicyAc = 18,
        ProcessorPowerPolicyCurrent = 22,
        ProcessorPowerPolicyDc = 19,
        SystemBatteryState = 5,
        SystemExecutionState = 16,
        SystemPowerCapabilities = 4,
        SystemPowerInformation = 12,
        SystemPowerPolicyAc = 0,
        SystemPowerPolicyCurrent = 8,
        SystemPowerPolicyDc = 1,
        SystemReserveHiberFile = 10,
        VerifyProcessorPowerPolicyAc = 20,
        VerifyProcessorPowerPolicyDc = 21,
        VerifySystemPolicyAc = 2,
        VerifySystemPolicyDc = 3
    }
}