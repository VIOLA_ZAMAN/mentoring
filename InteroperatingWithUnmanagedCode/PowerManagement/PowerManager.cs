﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace PowerManagement
{
    [ComVisible(true)]
    [Guid("e3950755-436a-4bc6-8650-9b599d4f9657")]
    [ClassInterface(ClassInterfaceType.None)]
    public class PowerManager: IPowerManager
    {
        public PowerManager()
        {
        }

        const int SystemPowerInformation = 12;
        const uint STATUS_SUCCESS = 0;
        public struct SYSTEM_POWER_INFORMATION
        {
            public uint MaxIdlenessAllowed;
            public uint Idleness;
            public uint TimeRemaining;
            public byte CoolingMode;
        }

        [DllImport("powrprof.dll")]
        static extern uint CallNtPowerInformation(
            int InformationLevel,
            IntPtr lpInputBuffer,
            int nInputBufferSize,
            [Out] out SYSTEM_POWER_INFORMATION spi,
            int nOutputBufferSize
        );

        [DllImport("powrprof.dll", SetLastError = true)]
        private static extern UInt32 CallNtPowerInformation(
        InformationLevel InformationLevel,
        IntPtr lpInputBuffer,
        UInt32 nInputBufferSize,
        [Out] IntPtr lpOutputBuffer,
        UInt32 nOutputBufferSize
        );

        [DllImport("powrprof.dll", SetLastError = true)]
        private static extern UInt32 CallNtPowerInformation(
            int InformationLevel,
            IntPtr lpInputBuffer,
            UInt32 nInputBufferSize,
            [Out] out ulong lpOutputBuffer,
            UInt32 nOutputBufferSize
        );

        [DllImport("powrprof.dll", SetLastError = true)]
        private static extern UInt32 CallNtPowerInformation(
            int InformationLevel,
            bool lpInputBuffer,
            UInt32 nInputBufferSize,
            IntPtr lpOutputBuffer,
            UInt32 nOutputBufferSize
        );

        [DllImport("Powrprof.dll", CharSet = CharSet.Auto, ExactSpelling = true)]
        public static extern bool SetSuspendState(bool hiberate, bool forceCritical, bool disableWakeEvent);

        public SYSTEM_BATTERY_STATE GetSystemBatteryState()
        {
            IntPtr state = Marshal.AllocCoTaskMem(Marshal.SizeOf(typeof(SYSTEM_BATTERY_STATE)));
            try
            {
                uint retval = CallNtPowerInformation(InformationLevel.SystemBatteryState,
                    IntPtr.Zero, 0, state, (UInt32) Marshal.SizeOf(typeof(SYSTEM_BATTERY_STATE)));
                SYSTEM_BATTERY_STATE btState = (SYSTEM_BATTERY_STATE) Marshal.PtrToStructure(state,
                    typeof(SYSTEM_BATTERY_STATE));
                if (retval == STATUS_SUCCESS)
                    return btState;
                return btState;
            }
            finally
            {
                Marshal.FreeHGlobal(state);
            }
        }

        public SYSTEM_POWER_INFORMATION GetSystemPowerInformation()
        {
            SYSTEM_POWER_INFORMATION spi;
            uint retval = CallNtPowerInformation(
                SystemPowerInformation,
                IntPtr.Zero,
                0,
                out spi,
                Marshal.SizeOf(typeof(SYSTEM_POWER_INFORMATION))
            );

            if (retval == STATUS_SUCCESS)
                return spi;
            return spi;
        }

        public ulong GetLastSleepTime()
        {
            var size = Marshal.SizeOf(typeof(ulong));
            ulong state;
            uint retval = CallNtPowerInformation((int)InformationLevel.LastSleepTime, IntPtr.Zero, 0, out state,
                (UInt32)size);

            if (retval == STATUS_SUCCESS)
            {
                return state;
            }
            return 0;
        }

        public ulong GetLastWakeTime()
        {
            var size = Marshal.SizeOf(typeof(ulong));
            ulong state;
            uint retval4 = CallNtPowerInformation((int)InformationLevel.LastWakeTime, IntPtr.Zero, 0, out state,
                (UInt32)size);

            if (retval4 == STATUS_SUCCESS)
                return state;
            return 0;
        }

        public bool SetSuspendState(bool isHibernate)
        {
            return SetSuspendState(isHibernate, true, false);
        }
    }
    [ComVisible(true)]
    [Guid("e3950755-436a-4bc6-8950-9b599d4f9657")]
    public struct SYSTEM_BATTERY_STATE
    {
        [MarshalAs(UnmanagedType.I1)]
        public bool AcOnLine;
        [MarshalAs(UnmanagedType.I1)]
        public bool BatteryPresent;
        [MarshalAs(UnmanagedType.I1)]
        public bool Charging;
        [MarshalAs(UnmanagedType.I1)]
        public bool Discharging;
        public byte Spare0;
        public byte Spare1;
        public byte Spare2;
        public byte Spare3;
        public uint MaxCapacity;
        public uint RemainingCapacity;
        public int Rate;
        public uint EstimatedTime;
        public uint DefaultAlert1;
        public uint DefaultAlert2;
    }
    [ComVisible(true)]
    [Guid("e3950755-436a-4bc6-8650-9b599d4f9657")]
    enum InformationLevel
    {
        AdministratorPowerPolicy = 9,
        LastSleepTime = 15,
        LastWakeTime = 14,
        ProcessorInformation = 11,
        ProcessorPowerPolicyAc = 18,
        ProcessorPowerPolicyCurrent = 22,
        ProcessorPowerPolicyDc = 19,
        SystemBatteryState = 5,
        SystemExecutionState = 16,
        SystemPowerCapabilities = 4,
        SystemPowerInformation = 12,
        SystemPowerPolicyAc = 0,
        SystemPowerPolicyCurrent = 8,
        SystemPowerPolicyDc = 1,
        SystemReserveHiberFile = 10,
        VerifyProcessorPowerPolicyAc = 20,
        VerifyProcessorPowerPolicyDc = 21,
        VerifySystemPolicyAc = 2,
        VerifySystemPolicyDc = 3
    }
}
