﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace PowerManagement
{
    [ComVisible(true)]
    [Guid("d4069372-5451-4aee-bd4a-d8d110e9010b")]
    [InterfaceType(ComInterfaceType.InterfaceIsDual)]
    public interface IPowerManager
    {
        SYSTEM_BATTERY_STATE GetSystemBatteryState();

        PowerManager.SYSTEM_POWER_INFORMATION GetSystemPowerInformation();

        ulong GetLastSleepTime();

        ulong GetLastWakeTime();

        bool SetSuspendState(bool isHibernate);
    }
}
