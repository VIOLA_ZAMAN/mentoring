﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using PowerManagement;

namespace GUI
{
    public partial class Form1 : Form
    {
        KeyboardHook hook = new KeyboardHook();

        public Form1()
        {
            InitializeComponent();

            // register the event that is fired after the key press.
            hook.KeyPressed +=
                new EventHandler<KeyPressedEventArgs>(hook_KeyPressed);
            // register the control + alt + F12 combination as hot key.
            hook.RegisterHotKey(GUI.ModifierKeys.Control,
                Keys.F12);
        }

        void hook_KeyPressed(object sender, KeyPressedEventArgs e)
        {
            PowerManager powerManager = new PowerManager();
            // show the keys pressed in a label.
            MessageBox.Show(powerManager.GetSystemBatteryState().BatteryPresent.ToString());
            MessageBox.Show(e.Modifier.ToString() + " + " + e.Key.ToString());
        }
    }
}
