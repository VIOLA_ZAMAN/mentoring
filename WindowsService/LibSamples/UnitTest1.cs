﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MigraDoc.DocumentObjectModel;
using System.IO;
using MigraDoc.DocumentObjectModel.Shapes;
using MigraDoc.Rendering;
using ZXing;
using System.Drawing;

namespace LibSamples
{
	[TestClass]
	public class UnitTest1
	{
		private string inDir = @".\Images";
		private string outFile = "out.pdf";

		[TestMethod]
		public void MigraSemple()
		{
			var document = new Document();
			var section = document.AddSection();

			foreach (var file in Directory.EnumerateFiles(inDir))
			{
				var img = section.AddImage(file);

				img.RelativeHorizontal = RelativeHorizontal.Page;
				img.RelativeVertical = RelativeVertical.Page;

				img.Top = 0;
				img.Left = 0;

				img.Height = document.DefaultPageSetup.PageHeight;
				img.Width = document.DefaultPageSetup.PageWidth;

				section.AddPageBreak();
			}


			var render = new PdfDocumentRenderer();
			render.Document = document;

			render.RenderDocument();
			render.Save(outFile);
		}


		[TestMethod]
		public void XZing()
		{
			var reader = new BarcodeReader { AutoRotate = true };

			foreach (var file in Directory.EnumerateFiles(inDir))
			{
				var bmp = (Bitmap) Bitmap.FromFile(file);
				var result = reader.Decode(bmp);

				if (result != null)
					Console.WriteLine("{0} {1}", result.BarcodeFormat, result.Text);
			}
		}
	}
}
