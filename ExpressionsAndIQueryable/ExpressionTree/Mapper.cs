﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace ExpressionTree
{
    public class Mapper<TSource, TDestination>
    {
        readonly Func<TSource, TDestination> _mapFunction;
        internal Mapper(Func<TSource, TDestination> func)
        {
            _mapFunction = func;
        }
        public TDestination Map(TSource source)
        {
            return _mapFunction(source);
        }

    }

    public class MappingGenerator
    {
        public Mapper<TSource, TDestination> Generate<TSource, TDestination>()
        {
            var sourceParam = Expression.Parameter(typeof(TSource));
            var localVariable = Expression.Variable(typeof(TDestination), "result");

            var list = new List<Expression>();

            list.Add(Expression.Assign(localVariable, Expression.New(typeof(TDestination))));
            
            var sourceProp = typeof(TSource).GetProperties().ToList();
            var desttinationProp = typeof(TDestination).GetProperties().ToList();

            foreach (var info in desttinationProp)
            {
                var sp = sourceProp.FirstOrDefault(i => i.Name.Equals(info.Name));
                if (sp != null)
                {
                    list.Add(Expression.Assign(Expression.PropertyOrField(localVariable, info.Name), Expression.PropertyOrField(sourceParam, sp.Name)));
                }
            }
            list.Add(localVariable);

            BlockExpression blockExpression = Expression.Block(new [] { localVariable }, list);

            var mapFunction = Expression.Lambda<Func<TSource, TDestination>>(blockExpression, sourceParam );
            return new Mapper<TSource, TDestination>(mapFunction.Compile());
        }
    }
}