﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace ExpressionVisitor
{
    public class ReplaceParams : System.Linq.Expressions.ExpressionVisitor
    {
        private readonly Dictionary<string, string> _paramsDictionary;

        public ReplaceParams(Dictionary<string, string> paramsDictionary)
        {
            _paramsDictionary = paramsDictionary;
        }

        public Expression Modify(Expression expression)
        {
            return Visit(expression);
        }

        protected override Expression VisitLambda<T>(Expression<T> node)
        {
            return Expression.Lambda(Visit(node.Body));
        }
        
        protected override Expression VisitParameter(ParameterExpression b)
        {
            return Expression.Constant(_paramsDictionary[b.Name]);
        }

    }
}
