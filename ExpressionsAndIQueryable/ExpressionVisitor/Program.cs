﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace ExpressionVisitor
{
    class Program
    {
        static void Main(string[] args)
        {
            Expression<Func<int, int>> source_exp = (a) => a + (a + 1) * (a + 5) * (a - 1);
            var result_exp = (new Transformer().VisitAndConvert(source_exp, ""));


            Console.WriteLine(source_exp + " " + source_exp.Compile().Invoke(3));
            Console.WriteLine(result_exp + " " + result_exp.Compile().Invoke(3));

            Dictionary<string, string> paramsDictionary = new Dictionary<string, string>()
            {
                {"var1", "Hello"},
                {"var2", "World"}
            };
            Expression<Func<string, string, string>> action = (var1, var2) => var1 + var2;
            var expr = new ReplaceParams(paramsDictionary).Modify(action);
            Console.WriteLine(expr);
            Console.ReadKey();


        }
    }
}
