﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using Newtonsoft.Json;
using NLog;
using NLog.Config;
using NLog.Targets;
using PostSharp.Aspects;

namespace CodeRewritingLibrary
{
    [Serializable]
    public class LoggingAspectCodeRewriting : OnMethodBoundaryAspect
    {
        [NonSerialized]
        private Logger _logger;

        public Logger Logger
        {
            get
            {
                if (_logger == null)
                    _logger = InitLogger();
                return _logger;
            }
        }

        public void OnEntry(MethodExecutionArgs args)
        {
            Logger.Info(DateTime.Now);
            Logger.Info(string.Format("Start method :{0}", args.Method.Name));
            Logger.Info(GetSerializedArgs(args.Method.GetParameters(), args.Arguments));
        }

        public void OnException(MethodExecutionArgs args)
        {
            Logger.Error(args.Exception);
        }

        public void OnExit(MethodExecutionArgs args)
        {
            Logger.Info(DateTime.Now);
            Logger.Info(string.Format("Stop method :{0}", args.Method.Name));
        }

        public void OnSuccess(MethodExecutionArgs args)
        {
            var value = args.ReturnValue;
            Logger.Info(value == null ? "void" : JsonConvert.SerializeObject(value));
        }

        private string GetSerializedArgs(ParameterInfo[] parameterInfo, Arguments arguments)
        {
            StringBuilder result = new StringBuilder();
            if (!arguments.Any())
                return "Without args";

            int count = 0;
            foreach (var info in parameterInfo)
            {
                if (count > 0)
                    result.Append(System.Environment.NewLine);
                result.Append(info.Name + " : ");
                result.Append(System.Environment.NewLine);
                result.Append(JsonConvert.SerializeObject(arguments[count]));
                count++;
            }

            return result.ToString();
        }

        private Logger InitLogger()
        {
            var currentDir = Path.GetDirectoryName(Process.GetCurrentProcess().MainModule.FileName);
            var conf = new LoggingConfiguration();
            var fileTarget = new FileTarget()
            {
                Name = "Default",
                FileName = Path.Combine(currentDir, "log.txt"),
                Layout = "${date} ${message} ${onexception:inner=${exception:format=toString}}"
            };
            conf.AddTarget(fileTarget);
            conf.AddRuleForAllLevels(fileTarget);

            LogManager.Configuration = conf;

            return LogManager.GetCurrentClassLogger();
        }
    }
}
