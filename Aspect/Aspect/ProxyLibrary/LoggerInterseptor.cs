﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Castle.Core.Interceptor;
using Castle.DynamicProxy;
using Newtonsoft.Json;
using NLog;
using NLog.Config;
using NLog.Targets;

namespace ProxyLibrary
{
    public class LoggerInterseptor : IInterceptor
    {
        private readonly Logger _logger;
        public LoggerInterseptor()
        {
            var currentDir = Path.GetDirectoryName(Process.GetCurrentProcess().MainModule.FileName);
            var conf = new LoggingConfiguration();
            var fileTarget = new FileTarget()
            {
                Name = "Default",
                FileName = Path.Combine(currentDir, "log.txt"),
                Layout = "${date} ${message} ${onexception:inner=${exception:format=toString}}"
            };
            conf.AddTarget(fileTarget);
            conf.AddRuleForAllLevels(fileTarget);

            LogManager.Configuration = conf;

            _logger = LogManager.GetCurrentClassLogger();
        }

        public void Intercept(IInvocation invocation)
        {
            _logger.Info(DateTime.Now);
            _logger.Info(invocation.Method.Name);
            _logger.Info(GetSerializedArgs(invocation.Method.GetParameters(), invocation.Arguments));
            invocation.Proceed();
            var value = invocation.ReturnValue;
            _logger.Info(value == null ? "void" : JsonConvert.SerializeObject(value));

            invocation.ReturnValue = value;
        }

        private string GetSerializedArgs(ParameterInfo[] parameterInfo, object[] arguments)
        {
            StringBuilder result = new StringBuilder();
            if (!arguments.Any())
                return "Without args";

            int count = 0;
            foreach (var info in parameterInfo)
            {
                result.Append(info.Name + " : ");
                result.Append(System.Environment.NewLine);
                result.Append(JsonConvert.SerializeObject(arguments[count]));
                count++;
            }

            return result.ToString();
        }
    }
}
