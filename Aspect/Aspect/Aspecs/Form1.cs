﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CodeRewritingLibrary;

namespace Aspecs
{
    public partial class Form1 : Form
    {
        private Button changeItemBtn = new Button();

        // This DataGridView control displays the contents of the list.
        private DataGridView customersDataGridView = new DataGridView();

        // This BindingSource binds the list to the DataGridView control.
        private BindingSource customersBindingSource = new BindingSource();
        [LoggingAspectCodeRewriting]
        public Form1()
        {
            InitializeComponent();
            Load += Form1_Load;
            // Set up the "Change Item" button.
            this.changeItemBtn.Text = "Change Item";
            this.changeItemBtn.Dock = DockStyle.Bottom;
            this.changeItemBtn.Click +=
                new EventHandler(changeItemBtn_Click);
            this.Controls.Add(this.changeItemBtn);

            // Set up the DataGridView.
            customersDataGridView.Dock = DockStyle.Top;
            this.Controls.Add(customersDataGridView);
            this.Size = new Size(400, 200);
        }
        private void Form1_Load(object sender, EventArgs e)
        {
            bindingSource(getDemoCustomers());
            
            this.customersDataGridView.DataSource =
                this.customersBindingSource;

        }
        [LoggingAspectCodeRewriting]
        BindingList<DemoCustomer> getDemoCustomers()
        {
            BindingList<DemoCustomer> customerList = new BindingList<DemoCustomer>();
            customerList.Add(DemoCustomer.CreateNewCustomer());
            customerList.Add(DemoCustomer.CreateNewCustomer());
            customerList.Add(DemoCustomer.CreateNewCustomer());
            return customerList;
        }

        [LoggingAspectCodeRewriting]
        void bindingSource(BindingList<DemoCustomer> bindingList)
        {
            // Bind the list to the BindingSource.
            this.customersBindingSource.DataSource = bindingList;

        }

        // Change the value of the CompanyName property for the first 
        // item in the list when the "Change Item" button is clicked.
        void changeItemBtn_Click(object sender, EventArgs e)
        {
            // Get a reference to the list from the BindingSource.
            BindingList<DemoCustomer> customerList =
                this.customersBindingSource.DataSource as BindingList<DemoCustomer>;

            // Change the value of the CompanyName property for the 
            // first item in the list.
            customerList[0].CustomerName = "Tailspin Toys";
            customerList[0].PhoneNumber = "(708)555-0150";
        }
    }
}
