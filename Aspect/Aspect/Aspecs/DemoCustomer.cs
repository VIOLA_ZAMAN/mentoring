﻿using System;
using PostSharp.Patterns.Model;

namespace Aspecs
{
    [NotifyPropertyChanged]
    public class DemoCustomer
    {
        private string customerNameValue = string.Empty;
        // These fields hold the values for the public properties.
        private string phoneNumberValue = string.Empty;

        // This method is called by the Set accessor of each property.
        // The CallerMemberName attribute that is applied to the optional propertyName
        // parameter causes the property name of the caller to be substituted as an argument.


        // The constructor is private to enforce the factory pattern.
        private DemoCustomer()
        {
            customerNameValue = "Customer";
            phoneNumberValue = "(312)555-0100";
        }

        // This property represents an ID, suitable
        // for use as a primary key in a database.
        public Guid ID { get; } = Guid.NewGuid();

        public string CustomerName
        {
            get { return customerNameValue; }

            set
            {
                if (value != customerNameValue)
                {
                    customerNameValue = value;
                }
            }
        }

        public string PhoneNumber
        {
            get { return phoneNumberValue; }

            set
            {
                if (value != phoneNumberValue)
                {
                    phoneNumberValue = value;
                }
            }
        }

        // This is the public factory method.
        public static DemoCustomer CreateNewCustomer()
        {
            return new DemoCustomer();
        }
    }
}