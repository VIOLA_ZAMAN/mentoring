﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AspectProxy
{
    public class CustomerService
    {
        public BindingSource Source { get; set; }

        public virtual BindingList<DemoCustomer> getDemoCustomers()
        {
            BindingList<DemoCustomer> customerList = new BindingList<DemoCustomer>();
            customerList.Add(DemoCustomer.CreateNewCustomer());
            customerList.Add(DemoCustomer.CreateNewCustomer());
            customerList.Add(DemoCustomer.CreateNewCustomer());
            return customerList;
        }

        public virtual void bindingSource(BindingList<DemoCustomer> bindingList)
        {
            // Bind the list to the BindingSource.
            Source.DataSource = bindingList;
        }
    }
}
