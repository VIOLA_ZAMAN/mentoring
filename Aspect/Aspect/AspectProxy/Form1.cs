﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using Castle.DynamicProxy;
using ProxyLibrary;

namespace AspectProxy
{
    public partial class Form1 : Form
    {
        private readonly Button changeItemBtn = new Button();

        // This BindingSource binds the list to the DataGridView control.
        private readonly BindingSource customersBindingSource = new BindingSource();

        // This DataGridView control displays the contents of the list.
        private readonly DataGridView customersDataGridView = new DataGridView();

        public Form1()
        {
            InitializeComponent();
            Load += Form1_Load;
            // Set up the "Change Item" button.
            changeItemBtn.Text = "Change Item";
            changeItemBtn.Dock = DockStyle.Bottom;
            changeItemBtn.Click +=
                changeItemBtn_Click;
            Controls.Add(changeItemBtn);

            // Set up the DataGridView.
            customersDataGridView.Dock = DockStyle.Top;
            Controls.Add(customersDataGridView);
            Size = new Size(400, 200);
        }

        public virtual void Form1_Load(object sender, EventArgs e)
        {
            var generator = new ProxyGenerator();
            CustomerService customerService = generator.CreateClassProxy<CustomerService>(new LoggerInterseptor());
            customerService.Source = customersBindingSource;
            customerService.bindingSource(customerService.getDemoCustomers());

            customersDataGridView.DataSource =
                customersBindingSource;
        }

        

        // Change the value of the CompanyName property for the first 
        // item in the list when the "Change Item" button is clicked.
        public virtual void changeItemBtn_Click(object sender, EventArgs e)
        {
            // Get a reference to the list from the BindingSource.
            BindingList<DemoCustomer> customerList =
                customersBindingSource.DataSource as BindingList<DemoCustomer>;

            // Change the value of the CompanyName property for the 
            // first item in the list.
            customerList[0].CustomerName = "Tailspin Toys";
            customerList[0].PhoneNumber = "(708)555-0150";
        }
    }
}