﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Xsl;

namespace RSSTransformer
{
    class Program
    {
        static void Main(string[] args)
        {
            var xsl = new XslCompiledTransform();
            xsl.Load("C:\\Projects\\Mentoring\\AdvancedXML\\RSSTransformer\\XSLTFile1.xslt");

            xsl.Transform("C:\\Projects\\Mentoring\\AdvancedXML\\RSSTransformer\\books.xml", "C:\\Projects\\Mentoring\\AdvancedXML\\RSSTransformer\\result.xml");

        }
    }
}
