﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" 
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:msxsl="urn:schemas-microsoft-com:xslt"
                xmlns:bk="http://library.by/catalog"   
                exclude-result-prefixes="msxsl"
>
  <xsl:output method="xml" indent="yes"/>

  <xsl:template name="getMonth">
    <xsl:param name="name" />
    <xsl:choose>
        <xsl:when test="$name = '01'">Jan</xsl:when>
        <xsl:when test="$name = '02'">Feb</xsl:when>
        <xsl:when test="$name = '03'">Mar</xsl:when>
        <xsl:when test="$name = '04'">Apr</xsl:when>
        <xsl:when test="$name = '05'">May</xsl:when>
        <xsl:when test="$name = '06'">Jun</xsl:when>
        <xsl:when test="$name = '07'">Jul</xsl:when>
        <xsl:when test="$name = '08'">Aug</xsl:when>
        <xsl:when test="$name = '09'">Sep</xsl:when>
        <xsl:when test="$name = '10'">Oct</xsl:when>
        <xsl:when test="$name = '11'">Nov</xsl:when>
        <xsl:when test="$name = '12'">Dec</xsl:when>
        <xsl:otherwise>99</xsl:otherwise>
    </xsl:choose>
</xsl:template>

    <xsl:template match="/">
		<xsl:element name="rss" >
			<xsl:attribute name="version">2.0</xsl:attribute>
			<xsl:apply-templates/>
		</xsl:element>
	</xsl:template> 
  <xsl:template match="/bk:catalog">
		<xsl:element name="channel">      
      <xsl:element name="title">Books</xsl:element>
      <xsl:element name="link">http://my.safaribooksonline.com//</xsl:element>
      <xsl:element name="description">Books online</xsl:element>
			<xsl:apply-templates/>
		</xsl:element>
	</xsl:template> 
  <xsl:template match="bk:book/bk:author | bk:book/bk:publish_date | bk:book/bk:publisher | bk:book/bk:genre | bk:book/bk:isbn"></xsl:template>
  
  <xsl:template match="bk:book/bk:registration_date">
    <xsl:element name="pubDate">
      <xsl:variable name="monthName">
        <xsl:call-template name="getMonth">
          <xsl:with-param name="name" select="substring(node(),6,2)" />
        </xsl:call-template>
      </xsl:variable>
    <xsl:value-of select="concat(substring(./node(),9,2),' ',$monthName, ' ', substring(./node(),0,5), ' 00:00:00 GMT')"/>      
    </xsl:element>
  </xsl:template>
  
  <xsl:template match="bk:book/bk:title">
    <xsl:element name="title">
      <xsl:value-of select="node()"/>
    <xsl:apply-templates>
    </xsl:apply-templates>
    </xsl:element>
  </xsl:template>
  <xsl:template match="bk:book">
    <xsl:element name="item">
      <xsl:apply-templates select="@* | node()"/>
      <xsl:if xmlns:x="urn1" test="bk:genre='Computer' and bk:isbn">
        <xsl:element name="link">http://my.safaribooksonline.com//<xsl:value-of select="bk:isbn"/></xsl:element>
      </xsl:if>
    </xsl:element>
  </xsl:template>
  <xsl:template match="@* | node()">
    <xsl:apply-templates select="@* | node()"/>    
  </xsl:template>
</xsl:stylesheet>
