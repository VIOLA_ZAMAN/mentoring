﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Xsl;

namespace HTML
{
    class Program
    {
        static void Main(string[] args)
        {
            XsltSettings settings = new XsltSettings(false, true);
            
            XslCompiledTransform xslt = new XslCompiledTransform();
            xslt.Load("C:\\Projects\\Mentoring\\AdvancedXML\\HTML\\XSLTFile1.xslt", settings, new XmlUrlResolver());

            xslt.Transform("C:\\Projects\\Mentoring\\AdvancedXML\\HTML\\books.xml", "C:\\Projects\\Mentoring\\AdvancedXML\\HTML\\result.html");

        }
    }
}
