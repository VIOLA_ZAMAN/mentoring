﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0"
 xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt"
	xmlns:x="http://library.by/catalog"
  xmlns:date="http://yourdomain.org/dates-and-times"
	exclude-result-prefixes="msxsl"
                xmlns:exsl="http://exslt.org/common"
>
  <msxsl:script language="JScript" implements-prefix="date">
    <![CDATA[
		function  currentDate(){
    	var d = new Date();
		return d.getYear() + "-" + d.getMonth() + "-" + d.getDay();
		};
	]]>
  </msxsl:script>

  <xsl:output method="html"/>

  <xsl:template name="bookTemp">

    <xsl:param name="book" />
    <tr>
      <xsl:element name="td">
        <xsl:value-of xmlns:x="http://library.by/catalog" select="$book/x:author"/>
      </xsl:element>
      <xsl:element name="td">
        <xsl:value-of xmlns:x="http://library.by/catalog" select="$book/x:title"/>
      </xsl:element>
      <xsl:element name="td">
        <xsl:value-of xmlns:x="http://library.by/catalog" select="$book/x:publish_date"/>
      </xsl:element>
      <xsl:element name="td">
        <xsl:value-of xmlns:x="http://library.by/catalog" select="$book/x:registration_date"/>
      </xsl:element>
    </tr>
  </xsl:template>

  <xsl:template match="/">
    <HTML>
      <HEAD>
        <TITLE>
          Текущие фонды по жанрам
        </TITLE>
      </HEAD>
      <BODY>
        <xsl:element name="h1"  >
          <xsl:value-of select="date:currentDate()"/>
        </xsl:element>
        <xsl:variable name="genre-array">
          <Item>Fantasy</Item>
          <Item>Computer</Item>
          <Item>Romance</Item>
          <Item>Horror</Item>
          <Item>Science Fiction</Item>
        </xsl:variable>

        <xsl:variable  xmlns:x="http://library.by/catalog" name="books" select="x:catalog/x:book"></xsl:variable>

        <xsl:for-each select="msxsl:node-set($genre-array)/Item">
          <xsl:variable name="currentG" select="."></xsl:variable>
          <xsl:variable name="count" select="0"></xsl:variable>
          <xsl:element name="h2">
            <xsl:value-of select="$currentG"/>
          </xsl:element>
          <table style="width:50%">
            <tr>
              <th>Author</th>
              <th>Name</th>
              <th>Publish date</th>
              <th>Registration date</th>
            </tr>
            <xsl:for-each xmlns:x="http://library.by/catalog" select="$books">
              <xsl:if xmlns:x="http://library.by/catalog" test="x:genre=$currentG">
                <xsl:call-template name="bookTemp">
                  <xsl:with-param name="book"
													select="." />
                </xsl:call-template>
              </xsl:if>
            </xsl:for-each>
          </table>
          <xsl:element name="h1"  >
            <xsl:attribute name="style" >text-align:center;</xsl:attribute>
            <xsl:value-of select="count($books[x:genre=$currentG])"/>
          </xsl:element>
        </xsl:for-each>
        <xsl:element name="h1"  >
          <xsl:value-of select="count($books)"/>
        </xsl:element>
      </BODY>
    </HTML>
  </xsl:template>

</xsl:stylesheet>
