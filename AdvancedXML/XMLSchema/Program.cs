﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Schema;

namespace XMLSchema
{
    class Program
    {
        static void Main(string[] args)
        {
            XmlReaderSettings settings = new XmlReaderSettings();

            settings.Schemas.Add("http://library.by/catalog", "C:\\Projects\\Mentoring\\AdvancedXML\\XMLSchema\\booksSchema.xsd");
            settings.ValidationEventHandler +=
                delegate (object sender, ValidationEventArgs e)
                {
                    Console.WriteLine(e.Message);
                };

            settings.ValidationFlags = settings.ValidationFlags | XmlSchemaValidationFlags.ReportValidationWarnings;
            settings.ValidationType = ValidationType.Schema;
            XmlReader reader = XmlReader.Create("C:\\Projects\\Mentoring\\AdvancedXML\\XMLSchema\\books.xml", settings);
            while (reader.Read()) {
                Console.WriteLine(reader.Name);
            };
            Console.ReadKey();

        }
    }
}
